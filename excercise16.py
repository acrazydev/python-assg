# python excercise 16

# openeing a file in read and write mode
file = open("file2.txt", "r+")

print("file name :- ", file.name)
print("file mode :- ", file.mode)
print("reading the file original data :- ")

line = file.readline()
print("reading file %s:- " % (line))

# opening file twice in a program
print("Truncating file")
file.truncate()

print("trying to read file after truncate operation")
print(file.read)

print("Enter the text to be entered into the file")
line1 = input("Enter the line1 :-  ")
line2 = input("Enter the line2 :-  ")
line3 = input("Enter the line3 :-  ")
line4 = input("Enter the line4 :-  ")


file = open("file2.txt", "w")
print("writing into the file using append mode")
str = "I am writing into the file"

file.write(line1)
file.write("\n")

file.write(line2)
file.write("\n")

file.write(line3)
file.write("\n")

file.write(line4)
file.write("\n")

file.close()
